const {
  constructAst,
  parseHtmlTree,
  writeJsonFileSync,
  readdirSync,
  readFile,
} = require("./lib/index");

const OUTPUTS_PATH = "./outputs";
const INPUTS_PATH = "./inputs";

// for debugging 
const {
  getFoundTags,
} = require("./lib/parse-html-tree");

const files = readdirSync("./inputs");

async function parseFile(fileName) {
  try {
    const fileContent = await readFile(`./inputs/${fileName}`, "utf-8");
    const htmlAst = constructAst(fileContent);
    const results = parseHtmlTree(htmlAst);
    writeJsonFileSync(`${OUTPUTS_PATH}/${fileName}.content.json`, results);
  } catch (error) {
    console.error(error);
  }
}

files.forEach(parseFile);
