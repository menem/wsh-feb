const HTML = require('html-parse-stringify');
const {
    removeLineEndings,
    removeMultiSpaces,
} = require("./strings");

const normalizeHtmlString = (htmlString) => removeLineEndings(removeMultiSpaces(htmlString.trim()));

const constructAst = (htmlString) => {
    // normalize
    const normalHTMString = normalizeHtmlString(htmlString);

    // parse
    const HTML_AST = HTML.parse(normalHTMString);
    
    return HTML_AST[0];
};

exports.constructAst = constructAst;