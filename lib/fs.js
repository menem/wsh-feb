const jsonfile = require('jsonfile');
const util = require('util');
const fs = require('fs');

exports.readdirSync = fs.readdirSync;
exports.readFile = util.promisify(fs.readFile);
exports.writeJsonFileSync = writeJsonFileSync;

function writeJsonFileSync(path, json) {
    jsonfile.writeFileSync(path, json, { spaces: 2 });
}