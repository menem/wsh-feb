exports.parseHtmlTree = require("./parse-html-tree").parseHtmlTree;
exports.constructAst = require("./construct-ast").constructAst;

const { 
    readdirSync,
    readFile,
    writeJsonFileSync,
} = require("./fs");
exports.readdirSync = readdirSync;
exports.readFile = readFile
exports.writeJsonFileSync = writeJsonFileSync;

exports.mergeResults = require("./objects").mergeResults;