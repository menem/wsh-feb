function mergeResults(left, right) {
    let results = {};

    for (const key in left) {
        if (left.hasOwnProperty(key)) {
            if (!results[key]) {
                results[key] = [];
            }
            results[key] = results[key].concat(left[key])
        }
    }

    for (const key in right) {
        if (right.hasOwnProperty(key)) {
            if (!results[key]) {
                results[key] = [];
            }
            results[key] = results[key].concat(right[key])
        }
    }

    return results;
}

exports.mergeResults = mergeResults;
