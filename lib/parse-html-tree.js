const {
    mergeResults,
} = require("./objects");

let foundTags = {};

exports.getFoundTags = () => foundTags;
exports.parseHtmlTree = parseHtmlTree;

function parseHtmlTree(node, parent) {
    let results = {};

    if (typeof node.name === "string") {
        if (node.name in foundTags) {
            foundTags[node.name] += 1;
        } else {
            foundTags[node.name] = 1;
        }
    }

    if (Array.isArray(node.children)) {
        for (const child of node.children) {
            const parsedNode = parseHtmlTree(child, node);
            if (Object.keys(parsedNode).length) {
                results = mergeResults(results, parsedNode);
            }
        }
    }

    if (isSection(node, parent)) {
        let section = {
            subsection: node.children[0].content, // TODO: extract all the text from the node tree like p tags
        };

        section.paragraphs = getSectionParagraphsText(node, parent);

        if (!section.paragraphs || !section.paragraphs.length) {
            return {};
        }

        return { sections: [section] };
    } else if (isParagraph(node)) {
        node.isPicked = true;
        const text = getParagraphTreeText(node);
        return { paragraphs: [text] };
    }

    return results;
};


function isSection(node, parent) {
    const headerTags = ["h1", "h2", "h3", "h4", "h5", "h6"];
    const isHeader = headerTags.includes(node.name);
    return isHeader && parent.children.length > 1;
}

function isSubsection(node, parent) {
    return node.name === "a" && parent.children.length > 1;
}

function isParagraph(node) {
    return node.name === "p" && (!node.isPicked)
}

function getParagraphTreeText(node) {
    let text = "";
    if (Array.isArray(node.children)) {
        for (const child of node.children) {
            text += getParagraphTreeText(child);
        }
    }
    return node.content ? text + node.content : text + "";
}

function getSectionParagraphsText(node, parent) {
    let paragraphs = [];
    const nodeIndex = getNodeIndexInArray(node, parent);
    if (nodeIndex + 1 >= parent.children.length) {
        return paragraphs;
    }
    for (let index = nodeIndex + 1; index < parent.children.length; index++) {
        const currNode = parent.children[index];
        let hasNoMoreParagraphs = currNode.type === "tag" && currNode.name !== "p";
        if (hasNoMoreParagraphs) {
            break;
        }

        if (currNode.name === "p") {
            currNode.isPicked = true;
            const paragraph = getParagraphTreeText(currNode);
            paragraphs.push(paragraph);
        }
    }
    return paragraphs;
}

function getNodeIndexInArray(node, parent) {
    // NOTE: expected to work, becuase It's reference value not primitive
    return parent.children.indexOf(node);
}