exports.removeSpecialChars = (str) => str.replace(/[^a-zA-Z ]/g, "");

exports.removeLineEndings = (str) => str.replace(/\n/gi, "");

exports.removeMultiSpaces = (str) => str.replace(/\s\s+/g, ' ');
